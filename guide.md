## lancer la simulation

python3 <nom du fichier.py>

## parametres disponible 

conda activate env_fluidsim

ipython

from fluidsim.solvers.ns2d.solver import Simul

params = Simul.create_default_params()

## exemple
params.forcing
params.init_field

## charger les plots 

from fluidsim import load_sim_for_plot
sim = load_sim_for_plot()


## importer dans le projet
https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb/-/issues/1
hg add
hg st
hg commit -m "sim"
hg push



