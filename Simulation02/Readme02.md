## Simulation 02

Dans cette simulation nous avons utilisé le solveur qui <fluidsim.solvers.ns2d.strat.solver> résout la turbulence stratifiée 2D par la méthode spectrale.

## Cas etudier 
Nous avons initialisé la simulation avec un maillage grossier pour réduire le temps de calcul . une boîte carrée de L= 10 avec un maillage structuré de <M= 128 x 128 > a eté simuler pendant <t = 100 s> et une <viscosité  de 1e-06>

## Champs initial
Nous avons choisi <noise> comme condition initiale avec une vitesse maximale de <V_max = 2.0 > ce paramètre permet des creer un bruit numérique qui sera utilisé comme champ initial de vitesse.

## Forcage 
Dans cette simulation nous avons modifié les paramètres de forçage par rapport à la 1 re simulation, nous avons déplace la position de forçage vers le régime inertiel et nous avons élargi le domaine de forçage en changeant nkmin et nkmax, nous avons aussi augmenté l'intensité de forçage

## Sauvegarde des resultas .
Nous avons choisi un pas de temps de sauvegarde des données calculées par le code fluidsim de 1 s et 0.5 s selon la grandeur ensuite nous avons utilise un code python pour importer les résultats de simulations et sauvegarder les figures.


## Resulats et discusion 
Nous avons sauvegardé les champs de vitesses instantanées à différents t, les figures  illustrent la génération et le développement des tourbillons et des échelles de taille plus petites par rapport à la 1re  simulation et pareil des zones de recirculations, on remarque aussi différentes distributions des champs de vitesses aux différents instants ce qui montre que l'écoulement est instationnaire .

Les spectres 1D et 2D sont aussi sauvegardés le spectre 1D garde la même allure dans les 2 directions mais il est différent par rapport à la  1re simulation dans le pic à cause de la position du forçage et aussi la pente de -5/3. La zone inertielle a eté reduite à cause du forçage .

 la dissipation d'énergie et d'enstrophie en fonction du temps est aussi sauvegardée on remarque que ces deux grandeurs se comportent de la même manière en fonction du temps et tend vers 0 .



