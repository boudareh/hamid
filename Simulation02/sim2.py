import os

import fluiddyn as fld
import numpy as np

#from fluidsim.solvers.ns2d.bouss.solver import Simul
from fluidsim.solvers.ns2d.strat.solver import Simul


if "Test02" in os.environ:
    t_end = 2.0
    nh = 24
else:
    t_end = 100.0
    nh = 128

params = Simul.create_default_params()

params.short_name_type_run = "forcage"
params.output.sub_directory = "sim2"

params.oper.nx =  nh
params.oper.Lx =  Lh = 10
params.oper.ny = nh #nh//2
params.oper.Ly = 10 #Lh //2

params.oper.coef_dealiasing = 0.7

delta_x = Lh / nh
params.nu_8 = 1e-06 #1e-08 

params.time_stepping.t_end = t_end

params.init_fields.type = "noise" 
params.init_fields.noise.length = 1.0
params.init_fields.noise.velo_max = 2.0

params.forcing.enable = True
params.forcing.type = "tcrandom"

params.forcing.nkmax_forcing = 10.0 #
params.forcing.nkmin_forcing = 6.0  # 6.0
forcing_rate = 1.5    # 2.0 3.0
params.forcing.forcing_rate = forcing_rate
params.forcing.key_forced = "rot_fft"


params.output.periods_print.print_stdout = 0.5
params.output.periods_save.phys_fields = 1.0
params.output.periods_save.spectra = 1.0
params.output.periods_save.spatial_means = 0.05
params.output.periods_save.spect_energy_budg = 0.5
params.output.periods_save.spectra_multidim = 1.0
params.output.periods_save.spatiotemporal_spectra= 1.0
params.output.periods_save.temporal_spectra = 1.0
params.output.periods_save.increments = 1.0




#params.output.ONLINE_PLOT_OK = True
#params.output.spectra.HAS_TO_PLOT_SAVED = True
#params.output.spatial_means.HAS_TO_PLOT_SAVED = True
#params.output.spect_energy_budg.HAS_TO_PLOT_SAVED = True
#params.output.increments.HAS_TO_PLOT_SAVED = True
params.output.phys_fields.field_to_plot = "rot"
sim = Simul(params)

# field initialization in the script

sim.time_stepping.start()
sim.output.phys_fields.plot()

fld.show()
