## Simulation 01
Dans cette simulation nous avons utilisé le solveur qui <fluidsim.solvers.ns2d.strat.solver> résout la turbulence stratifiée 2D par la méthode spectrale.

## Cas etudié 
Nous avons initialisé la simulation avec un maillage grossier pour réduire le temps de calcul . une boîte carrée de L= 10 avec un maillage structuré de <M= 128 x 128 > a eté simuler pendant <t = 50 s> et une <viscosité  de 1e-06 >

## Champ initial
Nous avons choisi <noise> comme condition initiale avec une vitesse maximale de <V_max = 2.0 > ce paramètre permet des creer un bruit numérique qui sera utilisé comme champ initial de vitesse.

## Forcage .
Dans cette simulation nous avons choisi un forçage du type <tcrandom> et nous avons laissé les autres paramètres par défaut.

## Sauvegarde des résultas .
Nous avons choisi un pas de temps de sauvegarde des données calculées par le code fluidsim de 1 s et 0.5 s selon la grandeur ensuite nous avons utilise un code python pour importer les résultats de simulations et sauvegarder les figures.


## Resulats et discusion 
Nous avons sauvegardé les champs de vitesses instantanées à différents t, à t=0 correspond au champ initial de vitesse qui est un bruit non homogène, ensuite dans les autres instants les figures  illustrent la génération et le développement des tourbillons qui forment  des zones de recirculations après leur dissipation, et on remarque aussi différentes distributions des champs de vitesses aux différents instants ce qui montre que l'écoulement est instationnaire .
Les spectres 1D et 2D sont aussi sauvegardés le spectre 1D garde la même allure dans les 2 directions donc on peut dire qu'il s'agit d'une turbulence isotrope et uit aussi une loi de puissance de -5/3 .
 la dissipation d'énergie et d'enstrophie en fonction du temps est aussi sauvegardée on remarque que ces deux grandeurs se comportent de la même manière en fonction du temps...



