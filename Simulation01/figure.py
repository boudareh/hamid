import matplotlib.pyplot as plt
from pathlib import Path
from fluidsim import load

cas1 = Path(__file__).absolute().parent
figu = cas1 / "Simulation01"

path ='/home/cfd-user/Sim_data/simulation1/NS2D.strat_projet2d_128x128_S10x10_2023-01-08_03-31-09'

sim = load(path)

## initial Field
sim.output.phys_fields.plot(time =0, numfig=0, nb_contours=50, cmap="jet") 
fig = plt.gcf()
fig.savefig(figu / f"initial field.png")

##### instantaneous velocity
sim.output.phys_fields.plot(time =10, numfig=0, nb_contours=50, cmap="jet") 
fig = plt.gcf()
fig.savefig(figu / f"V_t10.png")

sim.output.phys_fields.plot(time =20, numfig=1, nb_contours=50, cmap="jet") 
fig = plt.gcf()
fig.savefig(figu / f"V_t20.png")

sim.output.phys_fields.plot(time =30, numfig=2, nb_contours=50, cmap="jet") 
fig = plt.gcf()
fig.savefig(figu / f"V_t30.png")

sim.output.phys_fields.plot(time =50, numfig=3, nb_contours=50, cmap="jet") 
fig = plt.gcf()
fig.savefig(figu / f"V_t50.png")


##Spectra

sim.output.spectra.plot1d(tmin=20)
fig = plt.gcf()
fig.savefig(figu / f"spectre1d.png")

sim.output.spectra.plot1d(tmin=20, coef_compensate = 5/3)
fig = plt.gcf()
fig.savefig(figu / f"spectre.png")

sim.output.spectra.plot2d()
fig = plt.gcf()
fig.savefig(figu / f"spectre2d.png")

### increment
sim.output.increments.plot()
fig = plt.gcf()
fig.savefig(figu / f"increment.png")
#### dissipation et energie
sim.output.spatial_means.plot()
fig = plt.gcf()
fig.savefig(figu / f"Dissipation_().png")

################spectra_multidim
sim.output.spectra_multidim.plot()
fig = plt.gcf()
fig.savefig(figu / f"spectra_multidim.png")

########### spect_energy_budg

sim.output.spect_energy_budg.plot()
fig = plt.gcf()
fig.savefig(f"spect_budg_kz_().png")

########animation
#sim.output.phys_fields.animate('vx', dt_frame_in_sec=0.3, dt_equations=0.25,tmin=10,tmax=50)
plt.show()


    


